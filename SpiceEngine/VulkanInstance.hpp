#pragma once

#include <vector>
#include <sstream>
#include <string>
#include <string_view>
#include <stdexcept>
#include <optional>
#include <utility>

#include "GLFWInstance.hpp"
#include "PhysicalDevice.hpp"
#include "vulkan_utils.hpp"


namespace fox::engine {
    class VulkanInstance {
    public:
        struct Config {
            std::vector<std::string> desired_extensions;
            const GLFWInstance* glfw_instance = nullptr;

            std::string application_name;
            std::string engine_name;

            Config& setDesiredExtensions(std::vector<std::string> desired_extensions) {
                this->desired_extensions = std::move(desired_extensions);
                return *this;
            }

            Config& setGLWFInstance(const GLFWInstance* glfw_instance) {
                this->glfw_instance = glfw_instance;
                return *this;
            }

            Config& setApplicationName(std::string application_name) {
                this->application_name = std::move(application_name);
                return *this;
            }

            Config& setEngineName(std::string engine_name) {
                this->engine_name = std::move(engine_name);
                return *this;
            }

            void validate() const {
                if (!glfw_instance)
                    throw std::runtime_error("GLFWInstance is not specified in VulkanInstance config");
                if (application_name.size() == 0)
                    throw std::runtime_error("ApplicationName is not specified in VulkanInstance config");
                if (engine_name.size() == 0)
                    throw std::runtime_error("EngineName is not specified in VulkanInstance config");
            }
        };

        VulkanInstance(const VulkanInstance&) = delete;
        VulkanInstance& operator=(const VulkanInstance&) = delete;
        VulkanInstance(VulkanInstance&&) = delete;
        VulkanInstance& operator=(VulkanInstance&&) = delete;

        VulkanInstance(const Config& config) {
            config.validate();
            std::vector<VkExtensionProperties> available_extensions = config.glfw_instance->enumerateInstanceExtensionProperties();
            for (auto& extension : config.desired_extensions) {
                if (!isExtensionSupported(available_extensions, extension)) {
                    std::stringstream ss;
                    ss << "Extension named '" << extension << "' is not supported";
                    throw std::runtime_error(ss.str());
                }
            }
            VkApplicationInfo application_info = {
                VK_STRUCTURE_TYPE_APPLICATION_INFO,
                nullptr,
                config.application_name.c_str(),
                VK_MAKE_VERSION(1, 0, 0),
                config.engine_name.c_str(),
                VK_MAKE_VERSION(1, 0, 0),
                VK_MAKE_VERSION(1, 0, 0)
            };
            std::vector<const char*> desired;
            desired.reserve(config.desired_extensions.size());
            for (auto& s : config.desired_extensions) {
                desired.push_back(s.c_str());
            }
            VkInstanceCreateInfo instance_create_info = {
                VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
                nullptr,
                0,
                &application_info,
                0,
                nullptr,
                static_cast<uint32_t>(config.desired_extensions.size()),
                desired.data()
            };
            instance_create_info.enabledLayerCount = 1;
            const char* layers[] = { "VK_LAYER_KHRONOS_validation" };
            instance_create_info.ppEnabledLayerNames = layers;
            VkResult result = vkCreateInstance(&instance_create_info, nullptr, &instance);
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not create Vulkan Instance");
            }
        }

        std::vector<PhysicalDevice> enumerateInstancePhysicalDevices() const {
            uint32_t devices_count = 0;
            VkResult result = VK_SUCCESS;
            result = vkEnumeratePhysicalDevices(instance, &devices_count, nullptr);
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not get the number of available physical devices");
            }

            std::vector<VkPhysicalDevice> available_devices;
            available_devices.resize(devices_count);
            result = vkEnumeratePhysicalDevices(instance, &devices_count, available_devices.data());
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not enumerate physical devices");
            }

            std::vector<PhysicalDevice> devices;
            devices.reserve(available_devices.size());
            for (auto& vk_phys_dev : available_devices) {
                devices.push_back(PhysicalDevice(vk_phys_dev));
            }
            return devices;
        }

        const VkInstance& getVkInstance() const {
            return instance;
        }

        ~VulkanInstance() {
            if (instance) {
                vkDestroyInstance(instance, nullptr);
                instance = VK_NULL_HANDLE;
            }
        }

    private:
        

        VkInstance instance;
    };
}