#pragma once

#include <functional>
#include <sstream>
#include <stdexcept>
#include <cstdint>
#include <vector>
#include <optional>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <algorithm>
#include <iterator>

#include "glfw.hpp"
#include "PhysicalDevice.hpp"
#include "VulkanInstance.hpp"
#include "vulkan_utils.hpp"


namespace fox::engine {
    class LogicalDevice {
    public:
        struct Config {
            struct QueueSelection {
                uint32_t group_id;
                VkQueueFlags desired_capabilities;
                int group_size;
                bool presentation_support;
            };
            
            const VulkanInstance* vulkan_instance;
            std::vector<std::string> desired_extensions;
            std::optional<PhysicalDevice> physical_device;
            std::vector<QueueSelection> desired_queues;
            VkPhysicalDeviceFeatures desired_features;
            std::function<bool (const VkPhysicalDeviceFeatures&)> features_checker;

            Config& setDesiredExtensions(std::vector<std::string> desired_extensions) {
                this->desired_extensions = std::move(desired_extensions);
                return *this;
            }

            Config& setPhysicalDevice(PhysicalDevice physical_device) {
                this->physical_device = std::move(physical_device);
                return *this;
            }

            Config& setDesiredFeatures(VkPhysicalDeviceFeatures desired_features) {
                this->desired_features = std::move(desired_features);
                return *this;
            }

            Config& setFeaturesChecker(const std::function<bool(const VkPhysicalDeviceFeatures&)>& features_checker) {
                this->features_checker = features_checker;
                return *this;
            }

            Config& selectQueues(uint32_t group_id, VkQueueFlags desired_capabilities, int queue_count,
                bool presentation_support=false) {
                if (presentation_support && !vulkan_instance)
                    throw std::runtime_error("Specify vulkan_instance before selecting queues with presentation support");
                desired_queues.push_back(
                    QueueSelection{ group_id, desired_capabilities, queue_count, presentation_support }
                );
                return *this;
            }

            Config& setVulkanInstance(const VulkanInstance* instance) {
                this->vulkan_instance = instance;
                return *this;
            }
            
            void validate() const {
                if (!physical_device)
                    throw std::runtime_error("PhysicalDevice must be specified in LogicalDevice config");
                std::unordered_set<uint32_t> used_ids;
                for (auto& selection : desired_queues) {
                    if (used_ids.find(selection.group_id) != used_ids.end())
                        throw std::runtime_error(
                            (std::stringstream() << "Group id " << selection.group_id << " is already taken").str()
                        );
                    used_ids.insert(selection.group_id);
                }
            }
        };

        LogicalDevice(const LogicalDevice&) = delete;
        LogicalDevice& operator=(const LogicalDevice&) = delete;

        LogicalDevice(LogicalDevice&& moved_ldev) : physical_device(std::move(moved_ldev.physical_device)) {
            device = moved_ldev.device;
            moved_ldev.device = VK_NULL_HANDLE;

            queue_groups = std::move(moved_ldev.queue_groups);
        }

        LogicalDevice& operator=(LogicalDevice&& moved_ldev) noexcept {
            if (this == &moved_ldev)
                return *this;

            reset_device();
            device = moved_ldev.device;
            moved_ldev.device = VK_NULL_HANDLE;

            queue_groups = std::move(moved_ldev.queue_groups);
            physical_device = std::move(moved_ldev.physical_device);
            return *this;
        }

        LogicalDevice(const Config& config) : physical_device(*config.physical_device) {
            config.validate();

            std::vector<VkExtensionProperties> available_extensions = config.physical_device->enumerateDeviceExtensionProperties();

            for (auto& extension : config.desired_extensions) {
                if (!isExtensionSupported(available_extensions, extension)) {
                    throw std::runtime_error(
                        (std::stringstream() << "Extension named '" <<
                            extension << "' is not supported by a physical device").str()
                    );
                }
            }
            if (config.features_checker && !config.features_checker(config.physical_device->getFeatures())) {
                throw std::runtime_error("Some features is not available");
            }

            std::vector<QueueRequest> queue_requests = selectQueues(config);
            std::vector<VkDeviceQueueCreateInfo> queue_create_infos;
            for (auto& qrequest : queue_requests) {
                queue_create_infos.push_back({
                    VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                    nullptr,
                    0,
                    qrequest.family_index,
                    static_cast<uint32_t>(qrequest.priorities.size()),
                    qrequest.priorities.data()
                });
            };

            std::vector<const char*> extensions;
            extensions.reserve(config.desired_extensions.size());
            std::transform(config.desired_extensions.begin(), config.desired_extensions.end(), 
                std::back_inserter(extensions), [](const std::string& s) {return s.c_str(); });
            
            VkDeviceCreateInfo device_create_info = {
                VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                nullptr,
                0,
                static_cast<uint32_t>(queue_create_infos.size()),
                queue_create_infos.data(),
                0,
                nullptr,
                static_cast<uint32_t>(extensions.size()),
                extensions.data(),
                &config.desired_features
            };

            VkResult result = vkCreateDevice(config.physical_device->getVkPhysicalDevice(), &device_create_info, nullptr, &device);
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not create logical device");
            }

            loadQueues();
        }

        struct QueueGroup {
            uint32_t family_index;
            uint32_t offset;
            std::vector<VkQueue> queues;
        };

        const QueueGroup& getQueueGroup(uint32_t group_id) const {
            return queue_groups.at(group_id);
        }

        ~LogicalDevice() {
            reset_device();
        }

        const PhysicalDevice& getPhysicalDevice() const {
            return physical_device;
        }

        const VkDevice& getVkDevice() const {
            return device;
        }

        

    private:
        void reset_device() {
            if (device) {
                vkDestroyDevice(device, nullptr);
                device = VK_NULL_HANDLE;
            }
        }

        struct QueueRequest {
            uint32_t family_index;
            std::vector<float> priorities;
        };
        
        std::vector<QueueRequest> selectQueues(const Config& config) {
            std::vector<QueueRequest> queues;
            std::unordered_map<uint32_t, int> used_families;
            const auto& queue_families = config.physical_device->getQueueGamilies();
            for (auto& selection : config.desired_queues) {
                bool found = false;
                for (uint32_t index = 0; index < static_cast<uint32_t>(queue_families.size()); ++index) {
                    if ((static_cast<int>(queue_families[index].queueCount) - used_families[index] >= selection.group_size) &&
                        (queue_families[index].queueFlags & selection.desired_capabilities) &&
                        !(selection.presentation_support && !glfwGetPhysicalDevicePresentationSupport(
                            config.vulkan_instance->getVkInstance(),
                            config.physical_device->getVkPhysicalDevice(),
                            index
                        ))
                    ) {
                        queue_groups[selection.group_id].family_index = index;
                        queue_groups[selection.group_id].offset = used_families[index];
                        queue_groups[selection.group_id].queues.resize(selection.group_size);

                        used_families[index] += selection.group_size;

                        found = true;
                        break;
                    }
                }
                if (!found)
                    throw std::runtime_error("Could not find suitable queues");
            }
            for (auto& it : used_families) {
                if (it.second > 0)
                    queues.push_back(QueueRequest{ it.first, std::vector<float>(it.second, 1.0f) });
            }
            return queues;
        }

        void loadQueues() {
            for (auto& it : queue_groups) {
                for (uint32_t index = 0; index < static_cast<uint32_t>(it.second.queues.size()); ++index) {
                    vkGetDeviceQueue(device, it.second.family_index, it.second.offset + index, &it.second.queues[index]);
                }
            }
        }

        std::unordered_map<uint32_t, QueueGroup> queue_groups;
        VkDevice device = VK_NULL_HANDLE;
        PhysicalDevice physical_device;
    };
}