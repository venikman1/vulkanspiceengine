#include <cstdint>
#include <iostream>
#include <exception>
#include <vector>

#include "GLFWInstance.hpp"
#include "VulkanInstance.hpp"
#include "LogicalDevice.hpp"
#include "SurfaceKHR.hpp"
#include "Swapchain.hpp"
#include "Pipeline.hpp"

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData) {

    std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

    return VK_FALSE;
}

int main() {
    try {
        fox::engine::GLFWInstance fox;
        std::cout << "Available extensions:" << std::endl;
        for (auto& ext : fox.enumerateInstanceExtensionProperties()) {
            std::cout << ext.extensionName << std::endl;
        }
        std::cout << std::endl;

        uint32_t count;

        uint32_t layerCount;
        vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

        std::vector<VkLayerProperties> availableLayers(layerCount);
        vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

        const char** extensions = glfwGetRequiredInstanceExtensions(&count);
        std::vector<std::string> desired_extensions;
        desired_extensions.reserve(count);
        std::cout << "Required extensions:" << std::endl;
        for (int i = 0; i < static_cast<int>(count); ++i) {
            std::cout << extensions[i] << std::endl;
            desired_extensions.push_back(extensions[i]);
        }
        desired_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        std::cout << std::endl;

        fox::engine::VulkanInstance holo(
            fox::engine::VulkanInstance::Config()
            .setApplicationName("SpiceEngineTest")
            .setEngineName("SpiceEngine")
            .setDesiredExtensions(desired_extensions)
            .setGLWFInstance(&fox)
        );
        
        auto devices = holo.enumerateInstancePhysicalDevices();

        for (auto& device : devices) {
            std::cout << "Device info:" << std::endl;
            std::cout << "deviceID = " << device.getProperties().deviceID << std::endl;
            std::cout << "deviceName = " << device.getProperties().deviceName << std::endl;
            std::cout << "Queue Families:" << std::endl;
            for (auto& q : device.getQueueGamilies()) {
                std::cout << "queueCount = " << q.queueCount << std::endl;
                std::cout << "queueFlags = " << q.queueFlags << std::endl;
            }
            std::cout << std::endl;
            std::cout << "Extensions:" << std::endl;
            for (auto& ext : device.enumerateDeviceExtensionProperties()) {
                std::cout << ext.extensionName << std::endl;
            }
            std::cout << std::endl;
        }

        VkPhysicalDeviceFeatures features = {};
        features.geometryShader = VK_TRUE;
        fox::engine::LogicalDevice ldev(
            fox::engine::LogicalDevice::Config()
            .setPhysicalDevice(devices[0])
            .setVulkanInstance(&holo)
            .selectQueues(0, VK_QUEUE_GRAPHICS_BIT, 1)
            .selectQueues(1, VK_QUEUE_COMPUTE_BIT, 1)
            .setFeaturesChecker([](const VkPhysicalDeviceFeatures& f) -> bool {return f.geometryShader; })
            .setDesiredFeatures(features)
            .setDesiredExtensions(std::vector{ std::string(VK_KHR_SWAPCHAIN_EXTENSION_NAME) })
        );
        
        GLFWwindow* w = fox.createWindow(800, 600, "Holo4ka is the best!");
        fox::engine::SurfaceKHR surface(holo, w);

        fox::engine::Swapchain swapchain(&ldev, surface, VK_PRESENT_MODE_MAILBOX_KHR);

        fox::engine::Pipeline pipeline(&ldev, swapchain, 0);

        while (!glfwWindowShouldClose(w))
        {

            /* Draw frame */
            pipeline.drawFrame(swapchain);

            /* Poll for and process events */
            glfwPollEvents();
        }
    }
    catch (const std::exception& exc) {
        std::cerr << "Caught exception: " << exc.what() << std::endl;
    }


    return 0;
}



