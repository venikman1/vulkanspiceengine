#pragma once

#include <vector>
#include <string>
#include <stdexcept>

#include "glfw.hpp"
#include "VulkanInstance.hpp"
#include "PhysicalDevice.hpp"

namespace fox::engine {
    class SurfaceKHR {
    public:
        
        SurfaceKHR(const VulkanInstance& instance, GLFWwindow* window) {
            VkResult r = glfwCreateWindowSurface(instance.getVkInstance(), window, nullptr, &surface);
            if (r != VK_SUCCESS) {
                throw std::runtime_error("Could not create surface");
            }
        }
        
        SurfaceKHR(const SurfaceKHR&) = delete;
        SurfaceKHR& operator=(const SurfaceKHR&) = delete;

        std::vector<VkPresentModeKHR> getPhysicalDeviceSurfacePresentModesKHR(const PhysicalDevice& physical_device) const {
            uint32_t present_modes_count = 0;
            VkResult result = VK_SUCCESS;
            result = vkGetPhysicalDeviceSurfacePresentModesKHR(
                physical_device.getVkPhysicalDevice(), surface, &present_modes_count, nullptr);
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not get the number of supported present modes");
            }

            std::vector<VkPresentModeKHR> present_modes(present_modes_count);
            result = vkGetPhysicalDeviceSurfacePresentModesKHR(
                physical_device.getVkPhysicalDevice(), surface, &present_modes_count, present_modes.data());
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not enumerate present modes");
            }
            return present_modes;
        }

        std::vector<VkSurfaceFormatKHR> getPhysicalDeviceSurfaceFormatsKHR(const PhysicalDevice& physical_device) const {
            uint32_t formats_count = 0;
            VkResult result = VK_SUCCESS;
            result = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device.getVkPhysicalDevice(), surface, &formats_count, nullptr);
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not get the number of supported surface formats.");
            }

            std::vector<VkSurfaceFormatKHR> surface_formats(formats_count);
            result = vkGetPhysicalDeviceSurfaceFormatsKHR(physical_device.getVkPhysicalDevice(), surface,
                &formats_count, surface_formats.data());
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not enumerate supported surface formats.");
            }
            return surface_formats;
        }

        bool isPresentModeSupported(const PhysicalDevice& physical_device, VkPresentModeKHR mode) const {
            for (auto& m : getPhysicalDeviceSurfacePresentModesKHR(physical_device)) {
                if (m == mode)
                    return true;
            }
            return false;
        }

        bool checkSupportedUsage(const PhysicalDevice& physical_device, VkImageUsageFlags usage) const {
            return (getPhysicalDeviceSurfaceCapabilitiesKHR(physical_device).supportedUsageFlags & usage) == usage;
        }

        VkSurfaceCapabilitiesKHR getPhysicalDeviceSurfaceCapabilitiesKHR(const PhysicalDevice& physical_device) const {
            VkSurfaceCapabilitiesKHR surface_capabilities;
            VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device.getVkPhysicalDevice(),
                surface, &surface_capabilities);
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not get the capabilities of a presentation surface");
            }
            return surface_capabilities;
        }

        const VkSurfaceKHR& getPresentationSurface() const {
            return surface;
        }

        ~SurfaceKHR() {
            if (surface) {
                /*vkDestroySurfaceKHR(instance, surface, nullptr);
                presentation_surface = VK_NULL_HANDLE;*/
            }
        }

    private:
        VkSurfaceKHR surface;
    };
}
