#pragma once

#include <stdexcept>
#include <utility>

#include "LogicalDevice.hpp"
#include "PhysicalDevice.hpp"
#include "SurfaceKHR.hpp"
#include "glfw.hpp"

namespace fox::engine {
    class Swapchain {
    public:

        Swapchain(const Swapchain&) = delete;
        Swapchain& operator=(const Swapchain&) = delete;

        Swapchain(Swapchain&& moved_swapchain): 
            swapchain_images(std::move(moved_swapchain.swapchain_images)),
            device(moved_swapchain.device)
        {
            swapchain = moved_swapchain.swapchain;
            moved_swapchain.swapchain = VK_NULL_HANDLE;
        }

        Swapchain& operator=(Swapchain&& moved_swapchain) {
            if (this == &moved_swapchain)
                return *this;

            reset_swapchain();
            swapchain = moved_swapchain.swapchain;
            moved_swapchain.swapchain = VK_NULL_HANDLE;

            swapchain_images = std::move(moved_swapchain.swapchain_images);
            device = std::move(moved_swapchain.device);
            return *this;
        }

        Swapchain(const LogicalDevice* device, const SurfaceKHR& surface, VkPresentModeKHR present_mode) : device(device) {
            auto capabilities = surface.getPhysicalDeviceSurfaceCapabilitiesKHR(device->getPhysicalDevice());
            auto formats = surface.getPhysicalDeviceSurfaceFormatsKHR(device->getPhysicalDevice());
            if (!surface.isPresentModeSupported(device->getPhysicalDevice(), present_mode))
                throw std::runtime_error("Surface doesn't support used present mode");
            VkSwapchainCreateInfoKHR swapchain_create_info = {
                VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
                nullptr,
                0,
                surface.getPresentationSurface(),
                capabilities.minImageCount,
                formats[0].format,
                formats[0].colorSpace,
                capabilities.currentExtent,
                1,
                VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
                VK_SHARING_MODE_EXCLUSIVE,
                0,
                nullptr,
                capabilities.currentTransform,
                VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
                present_mode,
                VK_TRUE,
                swapchain
            };
            used_format = formats[0].format;
            extent = capabilities.currentExtent;
            VkResult result = vkCreateSwapchainKHR(device->getVkDevice(), &swapchain_create_info, nullptr, &swapchain);
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not create a swapchain");
            }
            acquireSwapchainImages();
            acquireSwapchainImageViews();
        }
        
        const VkExtent2D& getExtent() const {
            return extent;
        }

        const VkFormat& getFormat() const {
            return used_format;
        }

        const std::vector<VkImage>& getImages() const {
            return swapchain_images;
        }
        
        const std::vector<VkImageView>& getImageViews() const {
            return swapchain_image_views;
        }

        const VkSwapchainKHR& getVkSwapchain() const {
            return swapchain;
        }

        ~Swapchain() {
            reset_swapchain();
        }
        
        uint32_t acquireNextImageKHR(VkSemaphore sem) const {
            uint32_t result;
            vkAcquireNextImageKHR(device->getVkDevice(), swapchain, UINT64_MAX, sem, VK_NULL_HANDLE, &result);
            return result;
        }

    private:
        void reset_swapchain() {
            if (swapchain != VK_NULL_HANDLE) {
                vkDestroySwapchainKHR(device->getVkDevice(), swapchain, nullptr);
                swapchain = VK_NULL_HANDLE;
            }
            for (auto imageView : swapchain_image_views) {
                vkDestroyImageView(device->getVkDevice(), imageView, nullptr);
            }
            swapchain_image_views.clear();
        }

        void acquireSwapchainImages() {
            if (swapchain_images.size())
                return;
            uint32_t images_count = 0;
            VkResult result = vkGetSwapchainImagesKHR(device->getVkDevice(), swapchain, &images_count, nullptr);
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not get the number of swapchain images");
            }
            swapchain_images.resize(images_count);
            result = vkGetSwapchainImagesKHR(device->getVkDevice(), swapchain, &images_count, swapchain_images.data());
            if (VK_SUCCESS != result) {
                throw std::runtime_error("Could not enumerate swapchain images");
            }
        }

        void acquireSwapchainImageViews() {
            if (swapchain_image_views.size())
                return;
            acquireSwapchainImages();
            swapchain_image_views.resize(swapchain_images.size());
            
            for (int i = 0; i < swapchain_image_views.size(); ++i) {
                VkImageViewCreateInfo createInfo{};
                createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
                createInfo.image = swapchain_images[i];
                createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
                createInfo.format = used_format;
                createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
                createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                createInfo.subresourceRange.baseMipLevel = 0;
                createInfo.subresourceRange.levelCount = 1;
                createInfo.subresourceRange.baseArrayLayer = 0;
                createInfo.subresourceRange.layerCount = 1;
                if (vkCreateImageView(device->getVkDevice(), &createInfo, nullptr, &swapchain_image_views[i]) != VK_SUCCESS) {
                    throw std::runtime_error("failed to create image views!");
                }
            }
        }

        VkSwapchainKHR swapchain = VK_NULL_HANDLE;
        const LogicalDevice* device;

        VkFormat used_format;
        VkExtent2D extent;
        std::vector<VkImage> swapchain_images;
        std::vector<VkImageView> swapchain_image_views;
    };
}
