#pragma once

#include <vector>

#include "GLFWInstance.hpp"

namespace fox::engine {

    class PhysicalDevice
    {
    public:
        PhysicalDevice(VkPhysicalDevice device) : device(device) {
            vkGetPhysicalDeviceFeatures(device, &features);
            vkGetPhysicalDeviceProperties(device, &properties);
            savePhysicalDeviceQueueFamilyProperties();
        }

        std::vector<VkExtensionProperties> enumerateDeviceExtensionProperties() const {
            std::vector<VkExtensionProperties> available_extensions;
            uint32_t extensions_count = 0;
            VkResult result = VK_SUCCESS;
            result = vkEnumerateDeviceExtensionProperties(device, nullptr, &extensions_count, nullptr);
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not get the number of device extensions");
            }

            available_extensions.resize(extensions_count);
            result = vkEnumerateDeviceExtensionProperties(device, nullptr, &extensions_count, available_extensions.data());
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not enumerate device extensions");
            }
            return available_extensions;
        }

        const VkPhysicalDeviceFeatures& getFeatures() const {
            return features;
        }

        const VkPhysicalDeviceProperties& getProperties() const {
            return properties;
        }

        const std::vector<VkQueueFamilyProperties>& getQueueGamilies() const {
            return queue_families;
        }

        const VkPhysicalDevice& getVkPhysicalDevice() const {
            return device;
        }

    private:
        void savePhysicalDeviceQueueFamilyProperties() {
            uint32_t queue_families_count = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_families_count, nullptr);
            if (queue_families_count == 0) {
                throw std::runtime_error("Could not get the number of queue families");
            }

            queue_families.resize(queue_families_count);
            vkGetPhysicalDeviceQueueFamilyProperties(device, &queue_families_count, queue_families.data());
            if (queue_families_count == 0) {
                throw std::runtime_error("Could not acquire properties of queue families");
            }
        }

        VkPhysicalDevice device = VK_NULL_HANDLE;
        VkPhysicalDeviceFeatures features;
        VkPhysicalDeviceProperties properties;

        std::vector<VkQueueFamilyProperties> queue_families;
    };
}