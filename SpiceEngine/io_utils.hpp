#pragma once

#include <fstream>
#include <vector>
#include <stdexcept>
#include <sstream>

namespace fox {
    static inline std::vector<char> read_file(const std::string& filename) {
        std::ifstream file(filename, std::ios::ate | std::ios::binary);

        if (!file.is_open()) {
            throw std::runtime_error(
                (std::stringstream() << "Cannot open a file \"" << filename << "\"").str()
            );
        }

        size_t file_size = (size_t) file.tellg();
        std::vector<char> buffer(file_size);
        file.seekg(0);
        file.read(buffer.data(), file_size);
        file.close();

        return buffer;
    }
}