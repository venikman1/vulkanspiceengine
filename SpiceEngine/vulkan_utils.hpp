#pragma once

#include <vector>
#include <string>
#include <string_view>

#include "glfw.hpp"


namespace fox::engine {
    static inline bool isExtensionSupported(const std::vector<VkExtensionProperties>& available, const std::string& extension) {
        for (auto& ext_prop : available) {
            if (std::string_view(ext_prop.extensionName) == extension)
                return true;
        }
        return false;
    }
}