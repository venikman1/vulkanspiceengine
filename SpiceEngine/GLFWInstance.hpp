#pragma once

#include <stdexcept>
#include <string>

#include "glfw.hpp"


namespace fox::engine {
    class GLFWInstance {
    public:
        GLFWInstance() {
            if (!glfwInit()) {
                throw std::runtime_error("Can't init GLFW");
            }
            if (!glfwVulkanSupported())
            {
                throw std::runtime_error("Vulkan is not supported"); 
            }
        }

        GLFWInstance(const GLFWInstance&) = delete;
        GLFWInstance& operator=(const GLFWInstance&) = delete;
        GLFWInstance(GLFWInstance&&) = delete;
        GLFWInstance& operator=(GLFWInstance&&) = delete;
        
        GLFWwindow* createWindow(int width, int height, const std::string& title) const {
            glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
            GLFWwindow* win = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
            if (!win) {
                throw std::runtime_error("Error with creating window");
            }
            return win;
        }

        //void loopWindow(GLFWwindow* window, const Pipeline& pipeline, const Swapchain& swapchain) const {
        //    while (!glfwWindowShouldClose(window))
        //    {

        //        /* Draw frame */
        //        pipeline.drawFrame(swapchain);

        //        /* Poll for and process events */
        //        glfwPollEvents();
        //    }
        //}

        std::vector<VkExtensionProperties> enumerateInstanceExtensionProperties() const {
            uint32_t extensions_count = 0;
            VkResult result;
            result = vkEnumerateInstanceExtensionProperties(nullptr, &extensions_count, nullptr);
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not get the number of Instance extensions");
            }

            std::vector<VkExtensionProperties> available_extensions;
            available_extensions.resize(extensions_count);
            result = vkEnumerateInstanceExtensionProperties(nullptr, &extensions_count, available_extensions.data());
            if (result != VK_SUCCESS) {
                throw std::runtime_error("Could not enumerate Instance extensions");
            }
            return available_extensions;
        }
        
        ~GLFWInstance() {
            glfwTerminate();
        }
    private:
    };
}